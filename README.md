# NXazint contribution

https://hdf5.gitlab-pages.esrf.fr/nexus/nxazint/classes/contributed_definitions/NXazint1d.html

https://hdf5.gitlab-pages.esrf.fr/nexus/nxazint/classes/contributed_definitions/NXazint2d.html

Submitted to the official *NeXus standard*:

https://github.com/nexusformat/definitions/pull/1395

Examples:

https://h5web.panosc.eu/h5grove?file=azint/lambda_integrated_maxiv.h5
